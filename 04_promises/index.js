const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

const init = async () => {
  console.log('Iterations will start in 1s');
  await sleep(1000);
  for (let i = 0; i < 5; i += 1) {
    console.log(i);
  }
}

init();
