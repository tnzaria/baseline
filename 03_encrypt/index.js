const atob = require('atob');

const parseJwt = (token) => (
  token.split('.').slice(0, 2).map(atob).map(JSON.parse)
)

const jwtToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJiMDhmODZhZi0zNWRhLTQ4ZjItOGZhYi1jZWYzOTA0NjYwYmQifQ.-xN_h82PHVTCMA9vdoHrcZxH-x5mb11y1537t3rGzcM';
console.log(parseJwt(jwtToken));