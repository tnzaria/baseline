# Умение настроить доступ к серверу
1. Сгенерировать пару SSH ключей в отдельной директории на локальном компьютере (не в .ssh)
![generate keys](01.png)
2. Запустить ubuntu server через multipass - https://multipass.run/ 
![launch ubuntu vm](02.png)
3. Добавить публичный ключ на сервер для доступа по SSH
4. Войти на сервер по ip-адресу, используя ключ для аутентификации 
![login via ssh](03.png)
![successful login](04.png)
